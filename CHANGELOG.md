# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.5 - 2024-11-08(14:02:50 +0000)

### Other

- Reduce regex usage in signal handling

## Release v0.5.4 - 2024-09-05(07:42:37 +0000)

### Other

- [NetModel] Connect via direct socket if possible

## Release v0.5.3 - 2024-03-01(11:13:10 +0000)

### Changes

- Rework startup

## Release v0.5.2 - 2023-11-22(18:02:22 +0000)

### Fixes

- Do not call setFlag or clearFlag if InterfaceReference path is empty

### Other

- If the InterfaceReference is initially empty, no subscription...

## Release v0.5.1 - 2023-05-09(12:29:46 +0000)

### Fixes

- Object and search paths must be dot terminated

## Release v0.5.0 - 2022-09-29(10:09:46 +0000)

### New

- Increase in unit test coverage

## Release v0.3.3 - 2022-04-14(07:28:21 +0000)

### Changes

- Make the Interface path prefixed

## Release v0.3.2 - 2022-03-25(16:21:03 +0000)

### Fixes

- [NetModel] Move populate code back to mod_netmodel

## Release v0.3.1 - 2022-01-14(11:31:55 +0000)

### Changes

- IP client startup does not always work properly

## Release v0.3.0 - 2022-01-10(20:41:09 +0000)

### New

- Create DHCPv4 Client mapping in NetModel

## Release v0.2.2 - 2022-01-06(08:30:41 +0000)

### Fixes

- Filtered parameters do not always work when a plugin starts before NetModel

## Release v0.2.1 - 2021-12-21(14:29:14 +0000)

### Changes

- Update parameters if NetModel Intf already exists

## Release v0.2.0 - 2021-12-09(09:33:19 +0000)

### New

- Allow for filtered InstancePaths

## Release v0.1.0 - 2021-11-16(15:20:10 +0000)

### New

- Integrate support for Ethernet links and interfaces in NetModel

