# mod-netmodel

Mod_netmodel offers a generic solution to add NetModel.Intf instances and to set some flags. An important use case is to load NetModel mibs.

## using mod-netmodel

To use mod-netmodel import the .so file and call the entry point.

```
%config{
}

import "mod-netmodel.so" as mnm;

%define {
    entry-point mnm.mod-netmodel_main;
    # plugin's entry-point
}
```

## mod-netmodel's config options

These options are supported:
- NetModel - csv list of hash tables that hold mod-netmodel options
- InstancePath - path to the template to monitor
- Tags - ssv list of flags to set
- Prefix - prefix to a create unique NetModel.Intf alias
- FilterParam - parameter to use as filter (if empty the filter is ignored)
- FilterValue - value to use as filter (if empty the filter is ignored)
- InterfaceReference - parameter that matches with NetModel.Intf.{i}.InterfacePath. If not empty, no NetModel instance will be added, only the flags are set.

## use cases

### add NetModel.Intf instance with flag(s)

For each instance of template <InstancePath>, add a NetModel.Intf instance with the same alias prefixed with 'Prefix' for uniqueness.

For example for 'Ethernet.Interface.wan.' the Netmodel instance 'Netmodel.Intf.ethIntf-wan.' is added with flag 'eth_intf' set.

```
%config {
    NetModel = "nm_intfs,nm_links";
    nm_intfs = {
        InstancePath = "Ethernet.Interface.",
        Tags = "eth_intf",
        Prefix = "ethIntf-"
    };

    nm_links = {
        InstancePath = "Ethernet.Link.",
        Tags = "eth_link netdev",
        Prefix = "ethLink-"
    };
}
```

### filter by parameter value

Maybe not all instances should create a NetModel.Intf. In this case a filter can be configured. An expression will be used to filter instances like 'Foo.Interface.[ FilterParam == "FilterValue" ]'. For each match a NetModel.Intf is created with the Alias and Flags set.

```
%config {
    NetModel = "nm_foo";
    nm_foo = {
        InstancePath = "Foo.Interface.",
        FilterParam = "Status",
        FilterValue = "Enabled",
        Tags = "foo",
        Prefix = "foo-"
    };
}
```

### use of wildcards in InstancePath

Sometimes you might want to add child instances of multiple parent instances. In this case a wildcard can be used in the InstancePath.
Mod_netmodel will add all child instances (VLANPort) for all instances of the parent (Bridge).
In NetModel the instance path will be filled in without wildcards.

```
nm_vlanport = {
    InstancePath = "Bridging.Bridge.*.VLANPort.",
    Tags = "vlanport",
    Prefix = "vlanport-"
};
```

Wildcards can also be used in combination with filters.
```
nm_bridge = {
    InstancePath = "Bridging.Bridge.*.Port.",
    FilterParam = "ManagementPort",
    FilterValue = "true",
    Tags = "bridge",
    Prefix = "bridge-"
};
```

### add flag(s) to existing NetModel.Intf instance

Some mibs, for example dhcpv4, should not create a NetModel.Intf instance. They build on top of an existing instance and only set flags.

In the case of DHCPv4.Client.wan. there is a parameter 'Interface' that is a reference to 'Device.IP.Interface.2.'. Mod_netmodel will look for an existing instance with 'NetModel.Intf.[InterfacePath == "Device.IP.Interface.2."]' to set the 'dhcpv4' flag.

If the InterfaceReference refers to a NetModel.Intf instance that does not exist mod-netmodel will wait until the instance is created.

```
%config {
    NetModel = "nm_dhcpv4";
    nm_dhcpv4 = {
        InstancePath = "DHCPv4.Client.",
        Tags = "dhcpv4",
        InterfaceReference = "Interface"
    };
}
```
