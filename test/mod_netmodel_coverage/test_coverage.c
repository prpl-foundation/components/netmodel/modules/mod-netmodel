/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "../common/mock.h"
#include "../../include_priv/mod_netmodel.h"
#include "test_coverage.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;
amxd_object_t* root_obj = NULL;

static const char* odl_config = "../common/mock.odl";
static const char* odl_mock_netmodel = "../common/mock_netmodel.odl";
static const char* odl_mock_ethernet = "mock_ethernet.odl";
static const char* odl_mock_bridging = "mock_bridging.odl";
static const char* odl_mock_bridging_populate = "mock_bridging_populate.odl";
static const char* odl_mock_extra_bridge_port = "mock_extra_bridge_port.odl";
static const char* odl_mock_dhcpv4 = "mock_dhcpv4.odl";
static const char* odl_mock_dhcpv4_populate = "mock_dhcpv4_populate.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "setFlag", AMXO_FUNC(_setFlag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "clearFlag", AMXO_FUNC(_clearFlag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "getIntfs", AMXO_FUNC(_getIntfs)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netmodel, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_ethernet, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_bridging, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_dhcpv4, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);

    assert_int_equal(_mod_netmodel_main(AMXO_START, &dm, &parser), 0);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_mod_netmodel_main(AMXO_STOP, &dm, &parser), 0);
    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_wait_for_start(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Ethernet.Interface.");
    amxd_trans_add_inst(&trans, 0, "before_start");
    amxd_trans_set_value(cstring_t, &trans, "Name", "no.start");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    obj = amxd_dm_findf(&dm, "NetModel.Intf.1.");
    assert_null(obj);

    amxp_sigmngr_trigger_signal(&dm.sigmngr, "app:start", NULL);

    obj = amxd_dm_findf(&dm, "NetModel.Intf.1.");
    assert_non_null(obj);

    amxd_trans_clean(&trans);
}

void test_add_interface(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    const char* value = NULL;
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "Ethernet.Interface.");
    amxd_trans_add_inst(&trans, 0, "ETH0");
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    obj = amxd_dm_findf(&dm, "NetModel.Intf.2.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "ethIntf-ETH0");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "eth0");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "ETH0");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Ethernet.Interface.2.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "eth_intf");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_add_link(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    const char* value = NULL;
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "Ethernet.Link.");
    amxd_trans_add_inst(&trans, 0, "ETH0");
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    obj = amxd_dm_findf(&dm, "NetModel.Intf.3.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "ethLink-ETH0");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "eth0");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "ETH0");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Ethernet.Link.1.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "eth_link");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_add_prefixed_link(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    const char* value = NULL;
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "Ethernet.Link.");
    amxd_trans_add_inst(&trans, 0, "ethLink-ETH1");
    amxd_trans_set_value(cstring_t, &trans, "Name", "eth1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    obj = amxd_dm_findf(&dm, "NetModel.Intf.4.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "ethLink-ETH1");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "eth1");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "ethLink-ETH1");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Ethernet.Link.2.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "eth_link");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_add_filtered_intf(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    const char* value = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_bridging_populate, root_obj), 0);
    handle_events();

    obj = amxd_dm_findf(&dm, "NetModel.Intf.5.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "bridge-lan");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "br-lan");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "lan");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Bridging.Bridge.1.Port.1.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "bridge");

    obj = amxd_dm_findf(&dm, "NetModel.Intf.6.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "port-ETH1");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "eth1");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "ETH1");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Bridging.Bridge.1.Port.2.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "port");

    obj = amxd_dm_findf(&dm, "NetModel.Intf.7.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "bridge-GUEST");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "br-guest");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "GUEST");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Bridging.Bridge.2.Port.1.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "bridge");

    obj = amxd_dm_findf(&dm, "NetModel.Intf.8.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "port-ETH2");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "eth2");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "ETH2");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.Bridging.Bridge.2.Port.2.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "port");

    amxc_var_clean(&params);
}

void test_add_existing_intf(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    const char* value = NULL;
    amxc_var_t params;
    amxd_trans_t trans;
    amxc_var_t* section_var = GETP_ARG(&parser.config, "nm_links");

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    // Add non existing intf
    assert_int_equal(nm_intf_add(section_var, "test-intf", "test-intf", "test.test-intf."), 0);

    // Check values
    obj = amxd_dm_findf(&dm, "NetModel.Intf.ethLink-test-intf.");
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "Alias");
    assert_non_null(value);
    assert_string_equal(value, "ethLink-test-intf");
    value = GETP_CHAR(&params, "Name");
    assert_non_null(value);
    assert_string_equal(value, "test-intf");
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "test-intf");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.test.test-intf.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "eth_link");

    // Change InterfacePath, Flags & InterfaceAlias
    amxd_trans_select_pathf(&trans, "NetModel.Intf.ethLink-test-intf.");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "");
    amxd_trans_set_value(cstring_t, &trans, "InterfaceAlias", "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Check new values
    amxc_var_clean(&params);
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "");

    //Add existing intf
    assert_int_equal(nm_intf_add(section_var, "test-intf", "test-intf", "test.test-intf."), 0);

    // Values should be ok again
    amxc_var_clean(&params);
    amxd_object_get_params(obj, &params, amxd_dm_access_protected);
    value = GETP_CHAR(&params, "InterfaceAlias");
    assert_non_null(value);
    assert_string_equal(value, "test-intf");
    value = GETP_CHAR(&params, "InterfacePath");
    assert_non_null(value);
    assert_string_equal(value, "Device.test.test-intf.");
    value = GETP_CHAR(&params, "Flags");
    assert_non_null(value);
    assert_string_equal(value, "eth_link");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_add_referenced_instance(UNUSED void** state) {
    amxd_object_t* obj = amxd_dm_findf(&dm, "NetModel.Intf.bridge-lan.");
    // Check if existing bridge interface does not yet have DHCPv4 flag
    char* current_flags = amxd_object_get_value(cstring_t, obj, "Flags", NULL);
    assert_non_null(current_flags);
    assert_string_equal(current_flags, "bridge");
    free(current_flags);

    // Add DHCPv4 Client instances
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_dhcpv4_populate, root_obj), 0);
    handle_events();

    // Check if existing bridge interface was found and flags was added
    current_flags = amxd_object_get_value(cstring_t, obj, "Flags", NULL);
    assert_non_null(current_flags);
    assert_string_equal(current_flags, "bridge dhcpv4");
    free(current_flags);

    // Add interface we are listining for
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_extra_bridge_port, root_obj), 0);
    handle_events();

    // check if dhcpv4 flag was applied
    obj = amxd_dm_findf(&dm, "NetModel.Intf.port-extra.");
    current_flags = amxd_object_get_value(cstring_t, obj, "Flags", NULL);
    assert_non_null(current_flags);
    assert_string_equal(current_flags, "port dhcpv4");
    free(current_flags);
}

