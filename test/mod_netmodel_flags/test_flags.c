/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "../common/mock.h"
#include "../../include_priv/mod_netmodel.h"
#include "test_flags.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;
amxd_object_t* root_obj = NULL;

static const char* odl_config = "../common/mock.odl";
static const char* odl_mock_netmodel = "../common/mock_netmodel.odl";
static const char* odl_mock_dhcpv6 = "mock_dhcpv6.odl";
static const char* odl_mock_dhcpv6_populate = "mock_dhcpv6_populate.odl";
static const char* odl_mock_ip_manager = "mock_ip-manager.odl";
static const char* odl_mock_ip_manager_populate = "mock_ip-manager_populate.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "setFlag", AMXO_FUNC(_setFlag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "clearFlag", AMXO_FUNC(_clearFlag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "getIntfs", AMXO_FUNC(_getIntfs)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_dhcpv6, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_netmodel, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_ip_manager, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_ip_manager_populate, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);

    assert_int_equal(_mod_netmodel_main(AMXO_START, &dm, &parser), 0);

    amxp_sigmngr_trigger_signal(&dm.sigmngr, "app:start", NULL);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_mod_netmodel_main(AMXO_STOP, &dm, &parser), 0);
    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_change_flags(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxd_object_t* obj2 = NULL;
    amxc_string_t buff;
    amxd_trans_t trans;
    char* name = NULL;
    char* name2 = NULL;

    amxc_string_init(&buff, 0);
    amxd_trans_init(&trans);

    // Add the DHCPv6 instances
    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_dhcpv6_populate, root_obj), 0);
    handle_events();

    // Check in the NetModel if there is an ip-wan and an ip-wan6 Intf
    obj = amxd_dm_findf(&dm, "NetModel.Intf.ip-wan.");
    assert_non_null(obj);

    obj = amxd_dm_findf(&dm, "NetModel.Intf.ip-wan6.");
    assert_non_null(obj);

    //Check for DHCPv6 instances
    obj = amxd_dm_findf(&dm, "DHCPv6.Client.1.");
    assert_non_null(obj);
    name = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
    obj2 = amxd_dm_findf(&dm, "NetModel.Intf.ip-wan.");
    assert_non_null(obj2);
    name2 = amxd_object_get_value(cstring_t, obj2, "InterfacePath", NULL);
    assert_string_equal(name, name2);
    free(name);
    free(name2);

    // Control the DHCPv6 flag in ip-wan
    name = amxd_object_get_value(cstring_t, obj2, "Flags", NULL);
    amxc_string_set(&buff, name);
    assert_int_not_equal(amxc_string_search(&buff, "dhcpv6", 0), -1);
    free(name);

    // Control the absence the DHCPv6 flag in ip-wan6
    obj = amxd_dm_findf(&dm, "NetModel.Intf.ip-wan6.");
    assert_non_null(obj);
    name = amxd_object_get_value(cstring_t, obj, "Flags", NULL);
    amxc_string_set(&buff, name);
    assert_int_equal(amxc_string_search(&buff, "dhcpv6", 0), -1);
    free(name);

    // Change the DHCPv6 client Interface parameter
    obj = amxd_dm_findf(&dm, "DHCPv6.Client.1.");
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Control the absence the DHCPv6 flag in ip-wan
    obj = amxd_dm_findf(&dm, "NetModel.Intf.ip-wan.");
    assert_non_null(obj);
    name = amxd_object_get_value(cstring_t, obj, "Flags", NULL);
    amxc_string_set(&buff, name);
    assert_int_equal(amxc_string_search(&buff, "dhcpv6", 0), -1);
    free(name);

    // Control the DHCPv6 flag in ip-wan6
    obj2 = amxd_dm_findf(&dm, "NetModel.Intf.ip-wan6.");
    name = amxd_object_get_value(cstring_t, obj2, "Flags", NULL);
    amxc_string_set(&buff, name);
    assert_int_not_equal(amxc_string_search(&buff, "dhcpv6", 0), -1);
    free(name);

    amxc_string_clean(&buff);
    amxd_trans_clean(&trans);
}
