/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <netmodel/client.h>

#include "mod_netmodel.h"

#define ME "nm_populate"
#define str_empty(x) (x == NULL || *x == 0)

typedef struct {
    amxb_subscription_t* subscription;
    char* path;
    char* interface;
    const amxc_var_t* config;
} subscription_ctx_t;

amxo_parser_t* mod_netmodel_parser = NULL;

static int nm_intf_setflag(const amxc_var_t* config, const char* path);

static bool nm_intf_exists(const char* nm_intf) {
    bool intf_exists = false;
    amxc_var_t data;
    amxc_string_t path;

    amxc_var_init(&data);
    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "NetModel.Intf.[Alias == '%s'].", nm_intf);

    amxb_get(netmodel_get_amxb_bus(), amxc_string_get(&path, 0), 0, &data, 1);
    if(!str_empty(GETP_CHAR(&data, "0.0.Alias"))) {
        intf_exists = true;
    }

    amxc_var_clean(&data);
    amxc_string_clean(&path);
    return intf_exists;
}

static void nm_intf_setflag_delayed(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    subscription_ctx_t* ctx = (subscription_ctx_t*) priv;
    const char* notification = GET_CHAR(data, "notification");
    const char* intf = NULL;

    when_null(notification, exit);

    if(strcmp(notification, "dm:instance-added") == 0) {
        intf = GETP_CHAR(data, "parameters.InterfacePath");
    } else {
        intf = GETP_CHAR(data, "parameters.InterfacePath.to");
    }
    when_null(intf, exit);
    when_null(ctx, exit);
    when_str_empty(ctx->interface, exit);
    when_false(strcmp(ctx->interface, intf) == 0, exit);

    nm_intf_setflag(ctx->config, ctx->path);

    amxb_subscription_delete(&ctx->subscription);
    free(ctx->path);
    free(ctx->interface);
    free(ctx);

exit:
    return;
}

static int create_subscription(const char* path,
                               const char* interface,
                               const amxc_var_t* config) {
    int rv = -1;
    subscription_ctx_t* ctx = (subscription_ctx_t*) calloc(1, sizeof(subscription_ctx_t));
    amxc_string_t str;

    amxc_string_init(&str, 0);

    when_null(ctx, exit);

    ctx->config = config;
    ctx->path = strdup(path);
    amxc_string_setf(&str, "%s", interface);
    ctx->interface = amxc_string_take_buffer(&str);

    amxc_string_set(&str, "notification in ['dm:instance-added', 'dm:object-changed'] && "
                    "contains('parameters.InterfacePath')");

    rv = amxb_subscription_new(&ctx->subscription, netmodel_get_amxb_bus(), "NetModel.Intf.",
                               amxc_string_get(&str, 0), nm_intf_setflag_delayed, ctx);
    if(AMXB_STATUS_OK != rv) {
        SAH_TRACEZ_ERROR(ME, "Failed to create subscription: '%s'", amxc_string_get(&str, 0));
        free(ctx->path);
        free(ctx->interface);
        free(ctx);
        ctx = NULL;
    }

exit:
    amxc_string_clean(&str);
    return rv;
}

static int nm_change_flag(const char* intf, amxc_var_t* config, bool set) {

    int rv = -1;
    amxc_string_t interface_ref;
    const char* netmodel_intf = NULL;
    amxc_var_t* ret = NULL;

    amxc_string_init(&interface_ref, 0);

    when_str_empty(intf, exit);

    // Get the Netmodel Interface
    amxc_string_setf(&interface_ref, "NetModel.Intf.[InterfacePath == '%s'].", intf);
    ret = netmodel_getIntfs(amxc_string_get(&interface_ref, 0), "", netmodel_traverse_this);
    netmodel_intf = GET_CHAR(ret, "0");
    when_str_empty_trace(netmodel_intf, exit, ERROR, "No match found for '%s'",
                         amxc_string_get(&interface_ref, 0));

    // Set the new flags
    if(set) {
        netmodel_setFlag(netmodel_intf, GET_CHAR(config, "Tags"), "", netmodel_traverse_this);
    } else {
        netmodel_clearFlag(netmodel_intf, GET_CHAR(config, "Tags"), "", netmodel_traverse_this);
    }

exit:
    amxc_var_delete(&ret);
    amxc_string_clean(&interface_ref);
    return rv;
}

static void interface_ref_changed_cb(UNUSED const char* signame,
                                     const amxc_var_t* const data,
                                     void* const priv) {
    amxc_var_t* config = (amxc_var_t*) priv;
    const char* from_intf = NULL;
    const char* to_intf = NULL;
    amxc_string_t param;

    amxc_string_init(&param, 0);

    amxc_string_setf(&param, "parameters.%s.from", GET_CHAR(config, "InterfaceReference"));
    from_intf = GETP_CHAR(data, amxc_string_get(&param, 0));    // Getting the "from" interface string

    amxc_string_setf(&param, "parameters.%s.to", GET_CHAR(config, "InterfaceReference"));
    to_intf = GETP_CHAR(data, amxc_string_get(&param, 0));  // Getting the "to" interface string

    // Removing the flag from the old interface of the NetModel
    nm_change_flag(from_intf, config, false);

    // Adding the flag to the new interface
    nm_change_flag(to_intf, config, true);

    amxc_string_clean(&param);
}

static char* clean_instance_path(const char* instance_path) {
    amxc_string_t path;
    char* clean_path = NULL;

    amxc_string_init(&path, 0);
    when_null(instance_path, exit);

    amxc_string_set(&path, instance_path);
    amxc_string_replace(&path, "\\.", ".", UINT32_MAX);

    clean_path = amxc_string_take_buffer(&path);

exit:
    amxc_string_clean(&path);
    return clean_path;
}

static int nm_intf_setflag(const amxc_var_t* config, const char* path) {
    amxc_string_t search_path;
    amxc_string_t filter;
    amxc_var_t ret;
    amxc_var_t ret2;
    const char* interface = NULL;
    const char* netmodel_intf = NULL;
    amxb_bus_ctx_t* plug_ctx = amxb_be_who_has(path);
    int rv = -1;

    amxc_var_init(&ret);
    amxc_var_init(&ret2);
    amxc_string_init(&search_path, 0);
    amxc_string_init(&filter, 0);

    // Creating a subscription to the object for any InterfaceReference change to the netmodel
    amxc_string_setf(&filter, "notification == 'dm:object-changed' && "
                     "contains('parameters.%s')",
                     GET_CHAR(config, "InterfaceReference"));
    rv = amxb_subscribe(plug_ctx, path, amxc_string_get(&filter, 0), interface_ref_changed_cb, (void*) config);

    if(rv == AMXB_STATUS_OK) {
        SAH_TRACEZ_INFO(ME, "Successfully started subscription for changes of '%s' on '%s'",
                        GET_CHAR(config, "InterfaceReference"), path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to start subscription for changes of '%s' on '%s'",
                         GET_CHAR(config, "InterfaceReference"), path);
    }

    // get the value to match with InterfacePath
    amxc_string_setf(&search_path, "%s%s", path, GET_CHAR(config, "InterfaceReference"));
    rv = amxb_get(plug_ctx, amxc_string_get(&search_path, 0), 0, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to get '%s'", amxc_string_get(&search_path, 0));

    interface = GETP_CHAR(&ret, "0.0.0");
    when_str_empty(interface, exit);

    // get the netmodel interface
    amxc_string_setf(&search_path, "NetModel.Intf.['%s' in InterfacePath].", interface);
    rv = amxb_get(netmodel_get_amxb_bus(), amxc_string_get(&search_path, 0), 0, &ret2, 5);
    netmodel_intf = amxc_var_key(GETP_ARG(&ret2, "0.0"));
    if((AMXB_STATUS_OK != rv) || (str_empty(netmodel_intf))) {
        SAH_TRACEZ_INFO(ME, "No match found for '%s', wait until instance added",
                        amxc_string_get(&search_path, 0));
        when_failed(create_subscription(path, interface, config), exit);
    } else {
        // set the flags
        netmodel_setFlag(netmodel_intf, GET_CHAR(config, "Tags"), "", netmodel_traverse_this);
    }

exit:
    amxc_string_clean(&search_path);
    amxc_string_clean(&filter);
    amxc_var_clean(&ret);
    amxc_var_clean(&ret2);
    return rv;
}

static void new_instance_cb(UNUSED const char* signame,
                            const amxc_var_t* const data,
                            void* const priv) {
    amxc_var_t* config = (amxc_var_t*) priv;
    amxc_string_t path;
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "%s%d.", GET_CHAR(data, "path"), GET_UINT32(data, "index"));

    if(str_empty(GET_CHAR(config, "InterfaceReference"))) {
        nm_intf_add(config, GETP_CHAR(data, "parameters.Alias"), GETP_CHAR(data, "parameters.Name"), amxc_string_get(&path, 0));
    } else {
        nm_intf_setflag(config, amxc_string_get(&path, 0));
    }

    amxc_string_clean(&path);
}


static void listen_on_instance_paths(amxc_var_t* config) {
    amxc_string_t expr;
    const char* filter_param = GET_CHAR(config, "FilterParam");
    const char* filter_value = GET_CHAR(config, "FilterValue");
    const char* instance_path = GET_CHAR(config, "InstancePath");
    char* clean_path = clean_instance_path(instance_path);
    amxb_bus_ctx_t* ctx = amxb_be_who_has(clean_path);
    int rv = -1;

    amxc_string_init(&expr, 0);

    when_null_trace(ctx, exit, ERROR, "No bus context found for %s", clean_path);

    if(!str_empty(filter_param) && !str_empty(filter_value)) {
        amxc_string_setf(&expr, "notification == 'dm:instance-added' && "
                         "path matches \"^%s$\" && parameters.%s == \"%s\"",
                         instance_path, filter_param, filter_value);
    } else {
        amxc_string_setf(&expr, "notification == 'dm:instance-added' && "
                         "path matches \"^%s$\"", instance_path);
    }

    rv = amxb_subscribe(ctx, clean_path, amxc_string_get(&expr, 0), new_instance_cb, config);
    if(rv == AMXB_STATUS_OK) {
        SAH_TRACEZ_INFO(ME, "Successfully started listening for new instances of %s", instance_path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to start listening for new instances of %s", instance_path);
    }

exit:
    amxc_string_clean(&expr);
    free(clean_path);
}

static void populate_with_initial_instance(amxc_var_t* config) {
    amxc_string_t expr;
    const char* filter_param = GET_CHAR(config, "FilterParam");
    const char* filter_value = GET_CHAR(config, "FilterValue");
    const char* instance_path = GET_CHAR(config, "InstancePath");
    char* clean_path = clean_instance_path(instance_path);
    amxb_bus_ctx_t* ctx = amxb_be_who_has(clean_path);
    int rv = -1;
    amxc_var_t data;
    amxc_var_t* instances = NULL;

    amxc_string_init(&expr, 0);
    amxc_var_init(&data);

    when_null_trace(ctx, exit, ERROR, "No bus context found for %s", clean_path);

    if(!str_empty(filter_param) && !str_empty(filter_value)) {
        amxc_string_setf(&expr, "%s[%s == \"%s\"].", clean_path, filter_param, filter_value);
    } else {
        amxc_string_setf(&expr, "%s*.", clean_path);
    }

    rv = amxb_get(ctx, amxc_string_get(&expr, 0), 0, &data, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to get %s", amxc_string_get(&expr, 0));

    instances = GET_ARG(&data, "0");
    when_null(instances, exit);

    amxc_var_for_each(instance, instances) {
        if(str_empty(GET_CHAR(config, "InterfaceReference"))) {
            nm_intf_add(config, GET_CHAR(instance, "Alias"), GET_CHAR(instance, "Name"), amxc_var_key(instance));
        } else {
            nm_intf_setflag(config, amxc_var_key(instance));
        }
    }

exit:
    amxc_var_clean(&data);
    amxc_string_clean(&expr);
    free(clean_path);
}

static void nm_populate(void) {
    amxc_string_t sections;
    amxc_llist_t llsections;

    amxc_string_init(&sections, 0);
    amxc_llist_init(&llsections);

    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize lib netmodel");

    amxc_string_set(&sections, GET_CHAR(&mod_netmodel_parser->config, "NetModel"));
    amxc_string_split_to_llist(&sections, &llsections, ',');
    amxc_llist_for_each(it, &llsections) {
        const char* section = amxc_string_get(amxc_string_from_llist_it(it), 0);
        amxc_var_t* section_var = GETP_ARG(&mod_netmodel_parser->config, section);

        if(section_var == NULL) {
            SAH_TRACEZ_ERROR(ME, "section %s does not exists in this config", section);
            continue;
        }

        amxc_var_add_key(cstring_t, section_var, "section_name", section);
        listen_on_instance_paths(section_var);
        populate_with_initial_instance(section_var);
    }

exit:
    amxc_string_clean(&sections);
    amxc_llist_clean(&llsections, amxc_string_list_it_free);
}

static void nm_available_cb(UNUSED const char* signame,
                            UNUSED const amxc_var_t* const data,
                            UNUSED void* const priv) {

    when_null_trace(amxb_be_who_has("NetModel."), exit, ERROR, "No NetModel bus context found");

    SAH_TRACEZ_INFO(ME, "NetModel available");
    nm_populate();

exit:
    return;
}

static void mod_init(UNUSED const char* signame,
                     UNUSED const amxc_var_t* const data,
                     UNUSED void* const priv) {
    int rv = -1;

    if(amxb_be_who_has("NetModel.") == NULL) {
        amxb_wait_for_object("NetModel.");
        rv = amxp_slot_connect_filtered(NULL, "^wait:NetModel\\.$", NULL, nm_available_cb, NULL);
        when_failed_trace(rv, exit, ERROR, "Failed to wait for NetModel to be available");
        SAH_TRACEZ_WARNING(ME, "Waiting for NetModel");
        goto exit;
    } else {
        nm_populate();
    }


exit:
    return;
}

int nm_intf_add(const amxc_var_t* config, const char* alias, const char* name, const char* path) {
    int rv = -1;
    amxc_var_t values;
    amxc_var_t ret;
    const char* prefix = GET_CHAR(config, "Prefix");
    amxc_string_t prefixed_alias;
    amxc_string_t full_path;

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);
    amxc_string_init(&prefixed_alias, 0);
    amxc_string_init(&full_path, 0);

    amxc_string_setf(&full_path, "Device.%s", path);
    amxc_string_set(&prefixed_alias, alias);

    if((prefix != NULL) && (*prefix != '\0')) {
        //if the alias does not start with the prefix, add it
        if(amxc_string_search(&prefixed_alias, prefix, 0) != 0) {
            amxc_string_prependf(&prefixed_alias, "%s", prefix);
        }
    }

    amxc_var_add_key(cstring_t, &values, "InterfacePath", amxc_string_get(&full_path, 0));
    amxc_var_add_key(cstring_t, &values, "InterfaceAlias", alias);

    if(nm_intf_exists(amxc_string_get(&prefixed_alias, 0))) {
        amxc_string_prependf(&prefixed_alias, "%s", "NetModel.Intf.");
        amxc_string_appendf(&prefixed_alias, "%s", ".");

        rv = amxb_set(netmodel_get_amxb_bus(), amxc_string_get(&prefixed_alias, 0), &values, &ret, 3);
        when_failed_trace(rv, exit, ERROR, "Failed to update InterfacePath/Alias for %s rv: %d",
                          amxc_string_get(&prefixed_alias, 0), rv);

        amxc_var_clean(&values);
        amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &values, "flag", GET_CHAR(config, "Tags"));

        // Use setFlag to add flags, setting them via amxb_set will overwrite existing flags
        netmodel_setFlag(amxc_string_get(&prefixed_alias, 0),
                         GET_CHAR(config, "Tags"),
                         "",
                         netmodel_traverse_this);
        SAH_TRACEZ_INFO(ME, "Updated NetModel interface %s", amxc_string_get(&prefixed_alias, 0));
    } else {
        amxc_var_add_key(cstring_t, &values, "Name", name);
        amxc_var_add_key(cstring_t, &values, "Flags", GET_CHAR(config, "Tags"));

        rv = amxb_add(netmodel_get_amxb_bus(),
                      "NetModel.Intf.",
                      0,
                      amxc_string_get(&prefixed_alias, 0),
                      &values,
                      &ret,
                      3);
        when_failed_trace(rv, exit, ERROR, "Failed to add %s to NetModel. rv: %d",
                          amxc_string_get(&prefixed_alias, 0), rv);
        SAH_TRACEZ_INFO(ME, "%s added to NetModel", amxc_string_get(&prefixed_alias, 0));
    }

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    amxc_string_clean(&prefixed_alias);
    amxc_string_clean(&full_path);
    return rv;
}

int _mod_netmodel_main(int reason,
                       UNUSED amxd_dm_t* dm,
                       amxo_parser_t* parser) {
    int rv = -1;
    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);
    switch(reason) {
    case AMXO_START:     // START
        mod_netmodel_parser = parser;
        rv = amxp_slot_connect(&dm->sigmngr, "app:start", NULL, mod_init, NULL);
        when_failed_trace(rv, exit, ERROR, "Failed to wait for app start");
        SAH_TRACEZ_WARNING(ME, "Waiting for app:start");
        break;
    case AMXO_STOP:     // STOP
        mod_netmodel_parser = NULL;
        netmodel_cleanup();
        rv = 0;
        break;
    }

exit:
    return rv;
}
